const fs = require("fs");
const sizeOf = require('image-size');


const srcBasePath = 'content/common/';
const publicPath = 'img/imagesBank/';
const albums = {}

function scanPath(path=''){
  fs.readdirSync(srcPath(path)).forEach((file)=>{
    if(isFolder(srcPath(path,file))){
      scanPath(`${path}${file}/`);
    } else {
      addToAlbum(file,path);
    }
  });
}
scanPath();
fs.writeFileSync(`generated/imagesBank.json`,JSON.stringify(albums),"utf8");


function addToAlbum(imageFile,albumPath){
  const size = getSize(srcPath(albumPath,imageFile));
  if(typeof albums[albumPath] === "undefined") albums[albumPath] = [];
  albums[albumPath].push({fullSizePath:imgWebPath(albumPath,imageFile),...size});
}
function srcPath(subPath,file=''){
  return `${srcBasePath}${publicPath}${subPath}${file}`;
}
function imgWebPath(albumPath,imageFile){
  return `${publicPath}${albumPath}${imageFile}`;
}
function isFolder(f){
  return fs.existsSync(f) && fs.lstatSync(f).isDirectory();
}
function getSize(f){
  const s = sizeOf(f);
  if(s.orientation===1) return {width:s.width,height:s.height};
  else return {width:s.height,height:s.width};
}
