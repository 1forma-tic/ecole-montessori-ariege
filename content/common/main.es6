import _1gallery from "../../node_modules/1gallery/1gallery.mjs";

const baseUrl = location.href.substring(0,location.href.lastIndexOf('/')+1);

function runJS(){

  // Popout keep_in_touch
  document.body.addEventListener("mouseleave",e=>{
    if(!localStorage.getItem('keep_in_touch_poped')){
      location.hash = 'keep_in_touch';
      sessionNav();
      localStorage.setItem('keep_in_touch_poped', true);
    }
  });
  // close disclamer
  if(localStorage.getItem('disclamer_closed')) document.getElementById("disclamer").style.display = "none";
  document.querySelector('#disclamer .close').addEventListener("click",e=>{
    document.getElementById("disclamer").style.display = "none";
    localStorage.setItem('disclamer_closed', true);
  });

function idNodesExtractor(dom = document) {
	const subPages = document.querySelectorAll("[id]");
	const pagesArray = Object.keys(subPages).map((k)=>subPages[k]);
	return pagesArray.filter(n=>n.id.indexOf('-') === -1);
}
try {
idNodesExtractor().forEach(l=>{
	const subTitle = l.querySelector('h1,h2,h3,h4,h5,h6');
	if(!subTitle) return;
	if(subTitle.innerHTML === subTitle.textContent) subTitle.innerHTML = `<a href="./${l.id}.html" data-print="${baseUrl}${l.id}.html" class="permalien" title="permalien">${subTitle.innerHTML} </a>`;
	const link = document.createElement("a");
	link.classList.add(`anchorLink`);
	link.href = `./${l.id}.html`;
	link.title="permalien";
	subTitle.appendChild(link);
});
} catch (e) { console.warn(e) }
function reagenceParticiperForPrint(){
	const programme = document.getElementById('programme');
	const participerScreen = document.getElementById('participer');
	if(!programme || !participerScreen) return;
	const participerPrint = participerScreen.cloneNode(true);
	participerScreen.classList.add('neverPrint');
	participerPrint.classList.add('onlyPrint');
	participerPrint.id = 'participerPrint';
	programme.parentNode.insertBefore(participerPrint, programme.nextSibling);
}
try{
reagenceParticiperForPrint();
} catch (e) { console.warn(e) }

let nextScroll;
const baseNode = document.createElement("base");
baseNode.setAttribute("href",baseUrl);
document.head.appendChild(baseNode);

function scrollTo(dest, duration = 500) {
	const frequence = 10;
	if(window.getComputedStyle(dest).position === 'fixed') return;
	const to = Math.max(0,Math.min(pageOffsetTop(dest)-66, document.body.clientHeight - window.innerHeight));
	let timeLeft = duration;
	let last = Date.now() - frequence;
	nextScroll = () => {
		if ((document.body.scrollTop || document.documentElement.scrollTop || 0) === to) {
			nextScroll = () => 0;
			return;
		}
		const now = Date.now();
		const elapsed = now-last;
		last = now;
		let currPos = window.pageYOffset + (to - window.pageYOffset) / (timeLeft / elapsed);
		timeLeft -= elapsed;
		if(timeLeft<0) currPos = window.pageYOffset + (to - window.pageYOffset);
		document.body.scrollTop = document.documentElement.scrollTop = currPos;
		if(timeLeft>0) setTimeout(nextScroll, frequence);
	};
	nextScroll();
}

function pageOffsetTop(node){
	return node.offsetTop + (node.offsetParent? pageOffsetTop(node.offsetParent):0);
}

function highlight(target) {
	target.classList.add("highlight");
	setTimeout(() => target.classList.remove("highlight"), 500);
}
function bgCloseBuilder(side){
	const bgClose = document.createElement("a");
	bgClose.classList.add(`bg${side}`);
	bgClose.classList.add("bgPopupClose");
	bgClose.href = "./#";
	bgClose.title = "Fermer";
	return bgClose;
}
try{
document.querySelectorAll('.subPage').forEach( popup => {
	const fermer = document.createElement("a");
	fermer.classList.add("close");
	fermer.href = "./#";
	fermer.title = "Fermer";
	fermer.innerHTML = "X";
  popup.insertBefore(fermer, popup.firstChild);
  popup.insertBefore(bgCloseBuilder('Top'), popup.firstChild);
  popup.insertBefore(bgCloseBuilder('Left'), popup.firstChild);
  popup.insertBefore(bgCloseBuilder('Right'), popup.firstChild);
  popup.insertBefore(bgCloseBuilder('Bottom'), popup.firstChild);
} );
} catch (e) { console.warn(e) }

const baseTitle = document.querySelector('meta[property="og:site_name"]').content;
const fullBaseTitle = ""+document.title;
function getTitle(hash){
	let title;
	if(hash !== 'top'){
		const target = document.getElementById(hash);
		if(!target) throw hash+" not found";
		const subTitle = target.querySelector('h1,h2,h3,h4,h5,h6');
		title = `${subTitle?subTitle.innerText:'Accueil'} ~ ${baseTitle}`;
	} else {
		title = fullBaseTitle;
	}
	return title;
}
try{
document.querySelectorAll('nav').forEach(nav=>nav.addEventListener('click',()=>document.querySelector('nav').classList.toggle("active")));
} catch (e) { console.warn(e) }
try{
let debutEasterEgg;
const easterEgg = () => {
  const fin = Date.now();
  if(fin-debutEasterEgg>5000) document.querySelectorAll('.disclaimer').forEach(n=>n.classList.add("ddisclaimer"));
};
document.querySelectorAll('.disclaimer').forEach(n=>n.addEventListener('mousedown',()=>{
  debutEasterEgg = Date.now();
  n.removeEventListener('mouseup',easterEgg);
  n.addEventListener('mouseup',easterEgg);
}));
} catch (e) { console.warn(e) }

document.querySelectorAll('a[href^="./"], a[href^="./"] *, a[href^="#"], a[href^="#"] *').forEach(internalNavLinking);
function internalNavLinking(linkNode) {
  linkNode.addEventListener('click',
    e => {
      if(!e.ctrlKey && !e.shiftKey && !e.altKey && !e.metaKey){
        //const link = ancestorMatching((m)=>m.href,e.target);
        try{
          const link = e.target.closest('[href]');
          const hash = getHash(link.href);
          document.title = getTitle(hash);
          e.preventDefault();
          e.stopPropagation();
          history.pushState({},document.title,hash?baseUrl+hash+'.html':baseUrl);
          sessionNav();
        } catch (e) {
          // classical link and not inline link
        }
      }
    }
  )
}
function ancestorMatching(matcher, domNode) {
	return matcher(domNode) ? domNode : ancestorMatching(matcher, domNode.parentNode);
}
function getHash(href = location.href){
	let hash = href.split('#')[1];
	if(hash) return hash;
	const currentPathMarker = href.lastIndexOf('/')+1;
	if(currentPathMarker === href.length) return '';
	const extPositionMarker = href.lastIndexOf('.');
	if(currentPathMarker>extPositionMarker) return '';
	return href.substring(currentPathMarker,extPositionMarker);
}
try{
window.addEventListener('popstate',sessionNav);
sessionNav();
} catch (e) { console.warn(e) }

function sessionNav(){
	document.querySelectorAll('.subPage').forEach( section => section.classList.add("inactive") );
	document.querySelectorAll(`.subPage.active`).forEach( link => link.classList.remove("active") );
	document.body.classList.remove("starter");
	document.body.classList.remove("noScroll");
	scrollBarShift(0);
	const widthWithScrollBar = document.body.offsetWidth;
	let hash = getHash();
	if(hash === "top") {
		scrollTo(document.body);
		hash = '';
	}
	document.title = getTitle(hash);
	history.replaceState({},document.title,hash?baseUrl+hash+'.html':baseUrl);
	if(hash){
		const activeTarget = document.getElementById(hash);
		activeTarget.classList.add("active");
		activeTarget.classList.remove("inactive");
		document.querySelectorAll(`a[href="#${hash}"]`).forEach(ref=>ref.classList.add("active") );
		if(activeTarget.classList.contains("subPage")){
			document.body.classList.add("noScroll");
			const widthNoScrollBar = document.body.offsetWidth;
			scrollBarShift(widthNoScrollBar-widthWithScrollBar);
			if(window.pageYOffset < window.innerHeight) document.body.classList.add("starter");
		} else {
			scrollTo(activeTarget);
			highlight(activeTarget);
		}
	}
	optiPrint(hash);
}
function optiPrint(hash) {
	document.querySelectorAll('.noPrint').forEach(n=>n.classList.remove('noPrint'));
	document.querySelectorAll('.print').forEach(n=>n.classList.remove('print'));
	if(!hash) return;
	const page = document.getElementById(hash);
	let ancestor = page;
	while (ancestor.parentNode){
		brothers(ancestor).forEach(n=>n.classList.add('noPrint'));
		ancestor = ancestor.parentNode;
	}
	if(hash === 'decouvrir'){
		page.querySelectorAll('.subPage').forEach(n=>n.classList.add('print'));
	}
}
function brothers(node) {
	const parent = node.parentNode;
	if(parent === node) return [];
	const bro = [];
	for(let i=0;i<parent.children.length;i++){
		if(parent.children[i] !== node) bro.push(parent.children[i]);
	}
	return bro;
}
function scrollBarShift(size){
	//document.querySelectorAll("nav, body>section").forEach(e => e.style.paddingRight = size+"px");
}



function getParentChildIndexOf(node){
	let child = node;
	let childIndex = 0;
	while( child.previousElementSibling != null ){
		child = child.previousElementSibling;
		childIndex++;
	}
	return childIndex;
}
try{
document.querySelectorAll('.large thead th').forEach( column => {
	column.addEventListener("click",e => {
		const childIndex = getParentChildIndexOf(e.target);
		document.querySelectorAll(`.large tr`).forEach( tr => {
			const node = tr.children[childIndex];
			node.classList.toggle("compact");
			if(!node.querySelectorAll(".short").length) node.innerHTML = `<span class="short"></span><span class="long">${node.innerHTML}</span>`;
		} );
	} );
} );
} catch (e) { console.warn(e) }

}
window.runJS = runJS;
