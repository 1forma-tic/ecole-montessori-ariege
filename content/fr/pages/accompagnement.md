title: Accompagnements
<<: !!inc/file ../temoignages.yml
-------------------
# Accompagnements

<img src="img/formation.jpg" class="portrait"/>


## Vous souhaitez faire de votre terrain un petit coin de Paradis ?

D'un balcon à une ferme en passant par des terrains collectifs, je vous accompagne pas à pas pour vous aider à vous poser les bonnes questions pour concevoir votre petit coin de Paradis.

Ainsi je vous aide à :
- clarifier vos besoins (autonomie, production alimentaire, loisirs, animaux...)
- identifier et valoriser les ressources locales (énergies, matériaux, connaissances, réseaux, etc.)
- intégrer la philosophie et les concepts fondamentaux de la permaculture
- suivre la méthodologie de design avec différents outils adaptés
- aménagement optimisé de ferme
- système de prise de décision sur une ferme avec le Management Holistique

**Attention :** Si vous souhaitez simplement installer un potager et rien changer à vos habitudes, faites plutôt appel à un jardinier-paysagiste. 
Dire que vous souhaitez installer un potager pour "faire de la permaculture" est comme dire que vous devenez bouddhiste simplement parce que vous méditez!

Pour moi la permaculture est toute une démarche de pensée sur laquelle je vous accompagne pas à pas AVEC VOUS sur VOTRE projet. Je ne vous délivre pas un plan clé en main.
Suivre un accompagnement avec moi, c'est comme une formation particulière pour qu'à terme vous soyez autonome dans la logique de conception de votre terrain. J'introduis les concepts à connaître au fur et à mesure.

Pour les designs de plus d'1ha je travaille en binôme avec Aurélie Monchany de Permagenta. A deux, nous nous complétons biens sur nos talents et compétences et formons une super synergie de type 1+1=3 !!

<center> 

<div><a class="button" href="contact.html">Collaborons !</a></div> 

</center>
</section>
<section>
  <h2>Témoignages</h2>
<%- include('../fragments/temoignages.ejs',{filter:"individuel"}); %>
</section>
<section>



## Comment je travaille?
1. Contact téléphonique : Vous m'expliquez votre projet dans les grandes lignes, je vérifie que je me sens en mesure de vous accompagner sur votre projet
2. Visite : Nous mettons au clair vos besoins, ce à quoi l'accompagnement va aboutir, les moyens de paiement (€, G1, troc...) et nous vérifions que nous souhaitons travailler ensemble
3. Accompagnement pas à pas : Nous programmons des sessions de travail ensemble, de préférence d'une traite.
Nous pouvons aussi convenir d'un accompagnement sur la durée où vous payez à chaque session ou par forfait.

