Pour moi ancrer le respect de la Nature dans la culture passe par la célébration ou ritualisation des étapes clés de la course du soleil dans le ciel. Ces étapes rythment les saisons, nos récoltes, notre fonctionnement biologique et psychologique, nos vies... Le monde moderne a voulu nous faire croire que nous en étions déconnectés... et pourtant savoir surfer sur les rythmes de lumière et d'obscurité peut nous être d'une telle richesse, nous apporter tant de force !

C'est pourquoi nous prenons le temps de célébrer comme nos ancêtres païens, ces rites qui ancrent profondément la spiritualité avec la vie des paysans.

Dès que possible, je co-organise donc des temps de célébration pour les 8 dates de la Roue de l'Année, avec les personnes enthousiastes qui se proposent.

![illu](img/wheel-of-the-year.jpg)


Ces temps sont des occasions de cercles de parole, de receuillement, de jeux, d'animations, de chants, de danses libres ou collectives, d'exercices d'écopsychologie, de méditations guidées et autres joyeusetés !
Vous êtes invités à apporter un plat végétarien à partager.

Ces événements sont toujours ni gratuits ni payants, mais à prix libre et conscient pour permettre l'accessibilité au plus grand nombre et à tous les âges !
