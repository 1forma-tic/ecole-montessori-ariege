<%
const albums = JSON.parse(include('../../../generated/imagesBank.json'));
const htmlAlbums = [];
for(let album in albums) {
  const htmlAlbum = [];
  const albumContent = albums[album];
  for(let albumItem of albumContent){
    htmlAlbum.push(`<img class="card ratio-${albumItem.width}-${albumItem.height}" src="${albumItem.fullSizePath}"/>`);
  }
  const gallery = include('../../../node_modules/1gallery/1gallery.ejs',{cards:htmlAlbum,classes:'banner zoomable autoplay-5s'});
  htmlAlbums.push(`<h2>Banque d'image : ${album}</h2>${gallery}`);
}
%>
<%- htmlAlbums.join('') %>
