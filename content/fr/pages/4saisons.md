title: Danser votre jardin au rythme des saisons
<<: !!inc/file ../dates.yml
eventFilterSlug: danser-jardin-rythme-saisons
-------------------
# Danser votre jardin au rythme des saisons
**Formation annuelle - Permaculture & Ecopsychologie**

<img src="img/4saisons.jpg" class="portrait"/>

1 année pour comprendre, accepter et se reconnecter au vivant :
- 4 week-ends de formation dans les Landes : cours théoriques, ateliers pratiques, design en permaculture, réfléxion consciente et célébrations de la saison.
- des outils pour designer VOTRE jardin et faire avancer votre projet entre les sessions.
- une expérience collaborative et soutenante inédite.


La crise du covid ne fait que révéler la fragilité de notre société.
Plutôt que de se sentir impuissant-e devant ce constat,
développer notre autonomie alimentaire tout en respectant le
Vivant permet d'agir concrètement pour se réapproprier notre
pouvoir d'agir, notre santé et notre connexion au Vivant.

### Objectif de la formation
Apprivoiser et danser avec le vivant pour concevoir et cultiver son jardin.

### Mais encore ....
• Connaître les gestes de base pour semer, rempoter, planter, récolter, cuisiner, conserver
• Savoir concevoir son jardin adapté à ses besoins, son sol, son climat
• Exercices pratiques pour développer une meilleure connexion au Vivant

# Dates à venir

<%
const now = (new Date()).getTime();
const futureEvents = events.filter( (e) => e.dateEnd.getTime()>now && e.type.titleSlug === eventFilterSlug );
futureEvents.sort( (a,b) => a.dateStart.getTime() - b.dateStart.getTime() );
futureEvents.forEach(event=>{
_%>
<%- include('../fragments/event.ejs',{myEvent:event}); %>
<% }); _%>
