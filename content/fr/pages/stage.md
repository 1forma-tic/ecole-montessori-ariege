title: Stages

<<: !!inc/file ../types_de_stages_et_d_evenements.yml
filterByTag: collectif

-------------------

# Stages

<a class="button right withPicto agenda" style="margin-right: 30px" href="agenda.html">Prochaines Dates...</a>

Pourquoi faire un stage?

La permaculture ne pouvant pas se résumer à des techniques, je considère que c'est surtout par la pratique
l'expérience, le vécu qu'elle peut s'intégrer réellement.

Pour ma part en tout cas, c'est à l'issue d'un CCP que j'ai pris conscience que malgré toutes mes lectures sur le
sujet auparavant, c'est vraiment en vivant le CCP que j'ai pu réellement intégrer l'intégralité du concept.
Cette intégration a été pour moi une réelle transformation d'épanouissement : alors que je pensais savoir mieux que
les autres tous les problèmes du monde et comment les résoudre, j'ai découvert des principes de sagesse qui
transcendent tous les dogmes. Cela m'a permis de voir le monde avec des yeux nouveaux, avec les clés fondamentales et
des exemples d'applications pratiques pour réellement faire partie d'une mouvance pour un monde meilleur. Ainsi je
pouvais réellement incarner, au-delà du concept, la célèbre citation de Gandhi :

"Sois le changement que tu souhaites voir dans le monde".

A travers mes stages, c'est une expérience que j'ai à coeur de vous faire vivre.
Une expérience construite des meilleurs ingrédients que j'ai pu glaner à travers mon parcours et les différents
stages auxquels j'ai pu assister.

Voici les différents formats d'événements que je propose :

Vous souhaitez...

<div class="flexArea">

<%
const filteredTypes = filterByTag ? types.filter( (t) => t.tags.indexOf(filterByTag) !== -1 ) : types;
filteredTypes.forEach(t=>{
_%>
<%- include('../fragments/eventTypePresentation.ejs',{evType:t}); %>
<% }); _%>

</div>
