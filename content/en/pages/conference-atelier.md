title: Conferences
-------------------
# Conferences

<img src="img/conference.jpg" class="portrait"/>

Conférences disponibles, format 1h30 + 30 min de questions réponses ou adaptables à la demande.

- "**Introduction à la permaculture**" : conférence qui permet de résumer la philosophie et les concepts derrière la permaculture.
- "**Expédition sur les chemins de la permaculture**" : mon parcours de découverte de la permaculture à travers le monde (France, Royaume-Uni, Allemagne, Australie, Inde, Cuba à travers la découverte de fermes, projets associatifs, éco-villages, initiatives urbaines...)
- "**Mon expérience de vie chez David Holmgren, co-fondateur de la permaculture, et sa femme Sue Denett, ma super-héroïne de la perma**" :  Une plongée dans un autre mode de vie, une autre philosophie de vie, intégrée à tous les aspects du quotidien. Une réelle inspiration qui permet d'illustrer un avenir souhaitable de sobriété heureuse.
- "**La permaculture au féminin, ou comment cultiver son jardin secret**"  : Témoignage de comment grâce à des méthodes d'observations de mon cycle j'ai trouvé bien plus qu'un moyen de contraception fiable et naturel, mais surtout une réelle libération de commencer à percer quelques mystères de mon corps et à réellement célébrer être une femme. (Public non féminin bienvenu).
