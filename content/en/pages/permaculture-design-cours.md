title: Cours de Conception en Permaculture (CCP)
-------------------
# Cours de Conception en Permaculture (CCP)

<iframe class="portrait" width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://peertube.iriseden.eu/videos/embed/2ff160b5-3461-4f05-abe5-c49036a02f5a" frameborder="0" allowfullscreen></iframe>


## CCP, qu'est-ce que c'est?

Un CCP ou Cours de Conception en Permaculture (PDC en anglais) est un curriculum standardisé de 72h minimum développé dans les années 80 par Bill Mollison,
le co-fondateur de la permaculture, dans le but de faciliter la diffusion de la permaculture par un format standardisé réplicable. Il est reconnu entre pairs au
niveau international. Le curriculum reprend les grandes lignes du livre "Permaculture, a designer's manual" de Bill Mollison.

## Quelle est la particularité de ce CCP par rapport aux autres?

Mon parcours de vie m'a amené à découvrir des innovations, des applications concrètes développés en entreprises,
dans les milieux associatifs ou les collectifs citoyens qui sont en parfait alignement avec l'éthique de la permaculture.
En plus de la base du curriculum je l'ai actualisé en y ajoutant ces nouveaux concepts qui me semblent fondamentaux
pour la construction d'un monde plus juste et en harmonie avec le vivant.
Il comprend 108h de formation et j'y développe en particulier les sujets de gouvernance, de monnaie libre et de l'informatique!

## Pour qui?

Aucun prérequis n'est nécessaire pour suivre un CCP qui sont accessibles à tous publics de 15 à 95 ans!
Les mineurs sont acceptés sous la responsabilité de leur tuteur légal.

## Pour quoi faire un CCP?

Le monde dans lequel on vit perd de son sens?
Vous vous sentez incompris de votre entourage, vous vous sentez seuls dans les questions existentielles que vous vous posez?
Vous avez peur pour l'avenir de l'humanité sur la planète?
Vous souhaitez contribuer à une transition de société mais vous ne savez pas par où commencer?
Vous envisagez une reconversion professionnelle?
Vous souhaitez apprendre des techniques concrètes que vous pourrez appliquer au quotidien?
Vous souhaiter intégrer la permaculture au-delà du théorique et l’appréhender de façon plus pratique à travers l'expérience?
Dans tous les cas, un CCP vous donnera des clés pour avancer sur ces questionnements.


<center>

[Téléchargez la brochure du prochain stage...](cours-design-permaculture.html)

[![Dossier CCP](img/CCPPetitPontbiel2020-page-de-garde.jpg)](img/CCP-Petit-Pontbiel-2020-Cours-Certifie-Permaculture.pdf)


[Je m'inscris !](https://framaforms.org/inscription-au-ccp-2020-kit-de-survie-pour-co-construire-demain-1580987260 "button")

</center>

## Programme

<div class="col">

### Contexte, éthique & principes

- Histoire et contexte de la naissance de la permaculture
- Ethiques et principes
- Principes écologiques de la forêt
- Permaculture et collapsologie
- Pensée systémique

### Design

- méthodologies de design (BOLRADIM, WASPA)
- analyse sectorielle
- échelle permanence (Yeomans, Darren Doherty)

### Ecologie

- Observer et identifier son sol
- Recettes de compostage
- Valoriser l'eau dans le paysage (Keyline design)
- Economiser l'eau au quotidien
- Epuration naturelle des eaux
- Fonctionnement physiologique des plantes
- Comprendre l'arbre pour le soigner
- Plantes bio-indicatrices et plantes sauvages comestibles & médicinales
- Principes d'agro-foresterie et agro-écologie
- Maraîchage manuel sur sol vivant
- Gestion d'un système forestier à risque incendie
- Intégration des animaux dans les écosystèmes

### Bâtiment et énergie

- Autonomie énergétique
- Bioclimatisme et éco-construction
- Urbanisme social

### Outils & technologies

- Outils adaptés
- L'informatique au service de l'humain

### Systèmes économiques

- Histoire et rôle de la monnaie
- Alternatives monétaires et monnaie libre

### Permaculture sociale et gouvernance

- Permaculture humaine
- Les bases de la santé
- Les bases de la nutrition naturopathique et cuisine
- Communication non violente (CNV)
- Systèmes de gouvernance en entreprise ou projets collectifs
- Systèmes de gouvernance citoyenne

</div>

<center>

[Téléchargez la brochure du prochain stage...](cours-design-permaculture.html)

[Je m'inscris !](https://framaforms.org/inscription-au-ccp-2020-kit-de-survie-pour-co-construire-demain-1580987260 "button")

</center>
